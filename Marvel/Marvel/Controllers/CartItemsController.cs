﻿using Marvel.Dto.Cart;
using Marvel.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CartItemsController : ControllerBase
    {
        private readonly ICartItemsService _cartItemsService;

        public CartItemsController(ICartItemsService cartItemsService)
        {
            _cartItemsService = cartItemsService;
        }

        [HttpPost("Add")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> AddToCart([FromBody] RequestAddToCartDto requestRemoveToCartDto)
        {
            await _cartItemsService.AddToCart(requestRemoveToCartDto);

            return NoContent();
        }

        [HttpDelete("Remove")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> RemoveToCart([FromQuery] RequestRemoveToCartDto requestAddToCartDto)
        {
            await _cartItemsService.RemoveToCart(requestAddToCartDto);

            return NoContent();
        }
    }
}
