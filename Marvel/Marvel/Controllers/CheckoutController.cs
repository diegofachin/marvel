﻿using Marvel.Dto.Checkout;
using Marvel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CheckoutController : ControllerBase
    {
        private readonly ICheckoutService _checkoutService;

        public CheckoutController(ICheckoutService checkouttService)
        {
            _checkoutService = checkouttService;
        }

        [HttpPut]
        [ProducesResponseType(typeof(ResponseCheckoutDto), StatusCodes.Status200OK)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<ResponseCheckoutDto>> Checkout(RequestCheckoutDto requestCheckoutDto)
        {
            return Ok(await _checkoutService.Checkout(requestCheckoutDto));
        }
    }
}
