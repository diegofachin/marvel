﻿using Marvel.Dto.Cart;
using Marvel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CartController : ControllerBase
    {

        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [HttpPost("Create")]
        [ProducesResponseType(typeof(ResponseCartDto), StatusCodes.Status200OK)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<ResponseCartDto>> CreateCart()
        {
            return Ok(await _cartService.CreateCart());
        }
        
    }
}
