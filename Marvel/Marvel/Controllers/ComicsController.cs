﻿using Marvel.Dto.Comics;
using Marvel.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Marvel.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ComicsController : ControllerBase
    {
        private readonly IComicsService _comicsService;

        public ComicsController(IComicsService comicsService)
        {
            _comicsService = comicsService;
        }

        [HttpGet]
        [ProducesResponseType((200), Type = typeof(ResponseComicsDto))]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public async Task<ActionResult<ResponseComicsDto>> FetchAll([FromQuery] RequestComicsDto requestComicsDto)
        {            
            return Ok(await _comicsService.ListComics(requestComicsDto));
        }
    }
}
