﻿using Marvel.Domain.Entity;
using Marvel.Domain.Repository.Generic;
using Marvel.Dto.Cart;
using Marvel.Dto.Checkout;
using Marvel.Services.Interfaces;
using System;
using System.Threading.Tasks;

namespace Marvel.Services
{
    public class CheckoutService : ICheckoutService
    {
        private readonly ICartService _cartService;

        public CheckoutService(ICartService cartService)
        {
            _cartService = cartService;
        }
        public async Task<ResponseCheckoutDto> Checkout(RequestCheckoutDto requestCheckoutDto)
        {
            var cart = _cartService.FindById(requestCheckoutDto.CartId).GetAwaiter().GetResult();
            cart.Open = requestCheckoutDto.Open;
            cart.DiscountCoupon = requestCheckoutDto.DiscountCoupon;
            cart.FinalAmount = requestCheckoutDto.FinalAmount;

            var updateCart = await _cartService.UpdateCart(cart);

            ResponseCheckoutDto responseCartDto = new()
            {
                CartId = updateCart.Id,
                Open = requestCheckoutDto.Open
            };

            return responseCartDto;
        }
    }
}
