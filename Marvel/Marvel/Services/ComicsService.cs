﻿using Marvel.Dto.Comics;
using Marvel.Services.Interfaces;
using System.Threading.Tasks;

namespace Marvel.Services
{
    public class ComicsService : IComicsService
    {
        private readonly IMarvelComicsService _marvelComicsService;

        public ComicsService(IMarvelComicsService marvelComicsService)
        {
            _marvelComicsService = marvelComicsService;
        }

        public async Task<ResponseComicsDto> ListComics(RequestComicsDto requestComicsDto)
        {
            return await _marvelComicsService.FetchAllComics(requestComicsDto);
        }
    }
}
