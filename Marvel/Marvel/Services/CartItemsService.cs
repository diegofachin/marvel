﻿using Marvel.Domain.Entity;
using Marvel.Domain.Repository.Generic;
using Marvel.Dto.Cart;
using Marvel.Services.Interfaces;
using System.Threading.Tasks;

namespace Marvel.Services
{
    public class CartItemsService : ICartItemsService
    {
        private readonly IRepository<CartItems> _cartItemsRepository;
        private readonly ICartService _cartService;

        public CartItemsService(IRepository<CartItems> cartItemsRepository,
            ICartService cartService)
        {
            _cartItemsRepository = cartItemsRepository;
            _cartService = cartService;
        }

        public async Task AddToCart(RequestAddToCartDto requestAddToCartDto)
        {
            CartItems cartItems = new()
            {
                Description = requestAddToCartDto.Description,
                ItemId = requestAddToCartDto.ItemId,
                CartId = requestAddToCartDto.CartId,
                Price = requestAddToCartDto.Price
            };

            await _cartItemsRepository.Create(cartItems);

            var cart = _cartService.FindById(requestAddToCartDto.CartId).GetAwaiter().GetResult();
            cart.Amount += requestAddToCartDto.Price;
            cart.FinalAmount += requestAddToCartDto.Price;

            await _cartService.UpdateCart(cart);

        }

        public async Task RemoveToCart(RequestRemoveToCartDto requestRemoveToCartDto)
        {
            await _cartItemsRepository.Delete(requestRemoveToCartDto.Id);

            var cart = _cartService.FindById(requestRemoveToCartDto.CartId).GetAwaiter().GetResult();
            cart.Amount -= requestRemoveToCartDto.Price;
            cart.FinalAmount -= requestRemoveToCartDto.Price;

            await _cartService.UpdateCart(cart);

        }
    }
}
