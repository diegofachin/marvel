﻿using Marvel.Dto.Comics;
using Marvel.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Marvel.Services
{
    public class MarvelComicsService : IMarvelComicsService
    {
        private readonly IConfiguration _configuration;

        public MarvelComicsService(IConfiguration configuration)
        {
            _configuration = configuration;

        }

        public async Task<ResponseComicsDto> FetchAllComics(RequestComicsDto requestComicsDto)
        {
            string ts = DateTime.Now.Ticks.ToString();
            string publicKey = _configuration.GetSection("MarvelComicsAPI:PublicKey").Value;
            string hash = GetHash(ts, publicKey, _configuration.GetSection("MarvelComicsAPI:PrivateKey").Value);

            var urlRequest = _configuration.GetSection("MarvelComicsAPI:BaseURL").Value +
                 $"comics?" +
                 $"apikey={publicKey}" +
                 $"&hash={hash}" +
                 $"&ts={ts}";

            urlRequest = requestComicsDto.Title is null ? urlRequest : $"{urlRequest}&title={requestComicsDto.Title}";

            RestClient restClient = new(urlRequest);
            IRestRequest restRequest = new RestRequest(Method.GET);
            IRestResponse restResponse = await restClient.ExecuteAsync(restRequest);
            restRequest.AddHeader("Accept", "application/json");

            return JsonConvert.DeserializeObject<ResponseComicsDto>(restResponse.Content);
        }

        private static string GetHash(string ts, string publicKey, string privateKey)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(ts + privateKey + publicKey);
            var generator = MD5.Create();
            byte[] bytesHash = generator.ComputeHash(bytes);

            return BitConverter.ToString(bytesHash).ToLower().Replace("-", String.Empty);
        }
    }
}
