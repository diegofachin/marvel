﻿using Marvel.Dto.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Services.Interfaces
{
    public interface ICartItemsService
    {
        Task AddToCart(RequestAddToCartDto requestAddToCartDto);
        Task RemoveToCart(RequestRemoveToCartDto requestRemoveToCartDto);
    }
}
