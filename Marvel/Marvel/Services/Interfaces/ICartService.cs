﻿using Marvel.Domain.Entity;
using Marvel.Dto.Cart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Services.Interfaces
{
    public interface ICartService
    {        
        Task<ResponseCartDto> CreateCart();

        Task<Cart> UpdateCart(Cart cart);

        Task<Cart> FindById(long id);
    }
}
