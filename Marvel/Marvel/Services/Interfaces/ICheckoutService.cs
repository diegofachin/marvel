﻿using Marvel.Dto.Checkout;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Services.Interfaces
{
    public interface ICheckoutService
    {
        Task<ResponseCheckoutDto> Checkout(RequestCheckoutDto requestCheckoutDto);
    }
}
