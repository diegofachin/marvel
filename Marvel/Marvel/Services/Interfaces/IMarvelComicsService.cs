﻿using Marvel.Dto.Comics;
using System.Threading.Tasks;

namespace Marvel.Services.Interfaces
{
    public interface IMarvelComicsService
    {
        Task<ResponseComicsDto> FetchAllComics(RequestComicsDto requestComicsDto);
    }
}
