﻿using Marvel.Dto.Comics;
using System.Threading.Tasks;

namespace Marvel.Services.Interfaces
{
    public interface IComicsService
    {
        Task<ResponseComicsDto> ListComics(RequestComicsDto requestComicsDto);
    };
}
