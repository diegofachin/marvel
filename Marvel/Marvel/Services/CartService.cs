﻿using Marvel.Domain.Entity;
using Marvel.Domain.Repository.Generic;
using Marvel.Dto.Cart;
using Marvel.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Services
{
    public class CartService : ICartService
    {
        private readonly IRepository<Cart> _cartRepository;

        public CartService(IRepository<Cart> cartRepository)
        {
            _cartRepository = cartRepository;
        }

        public async Task<ResponseCartDto> CreateCart()
        {
            Cart cart = new()
            {
                Open = true
            };

            var newCart = await _cartRepository.Create(cart);

            return new ResponseCartDto { CartId = newCart.Id};
        }

        public async Task<Cart> FindById(long id)
        {
            return await _cartRepository.FindById(id);
        }

        public async Task<Cart> UpdateCart(Cart cart)
        {
            return await _cartRepository.Update(cart);
        }
    }
}
