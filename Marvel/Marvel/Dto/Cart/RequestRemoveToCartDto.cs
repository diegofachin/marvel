﻿namespace Marvel.Dto.Cart
{
    public class RequestRemoveToCartDto
    {
        public long Id { get; set; }
        public long CartId { get; set; }
        public decimal Price { get; set; }
    }
}
