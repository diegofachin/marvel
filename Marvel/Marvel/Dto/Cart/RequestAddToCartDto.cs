﻿namespace Marvel.Dto.Cart
{
    public class RequestAddToCartDto
    {
        public int ItemId { get; set; }

        public string Description { get; set; }

        public int CartId { get; set; }

        public decimal Price { get; set; }
    }
}
