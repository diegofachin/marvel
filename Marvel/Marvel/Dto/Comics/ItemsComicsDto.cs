﻿using System.Collections.Generic;

namespace Marvel.Dto.Comics
{
    public class ItemsComicsDto
    {
        public int Offset { get; set; }

        public int Limit { get; set; }

        public int Total { get; set; }

        public int Count { get; set; }

        public List<ComicsDto> Results { get; set; }
    }
}
