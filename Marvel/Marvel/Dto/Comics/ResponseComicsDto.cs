﻿namespace Marvel.Dto.Comics
{
    public class ResponseComicsDto
    {
        public ItemsComicsDto Data { get; set; }
    }
}
