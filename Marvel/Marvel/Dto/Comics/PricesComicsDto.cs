﻿namespace Marvel.Dto.Comics
{
    public class PricesComicsDto
    {
        public string Type { get; set; }
        public decimal Price { get; set; }
    }
}
