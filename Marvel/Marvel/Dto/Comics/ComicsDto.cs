﻿using System.Collections.Generic;

namespace Marvel.Dto.Comics
{
    public class ComicsDto
    {
        public int Id { get; set; }

        public int DigitalId { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public List<PricesComicsDto> Prices { get; set; }
    }
}
