﻿namespace Marvel.Dto.Checkout
{
    public class RequestCheckoutDto
    {
        public int CartId { get; set; }

        public string DiscountCoupon { get; set; }

        public decimal FinalAmount { get; set; }

        public bool Open { get; set; }

    }
}
