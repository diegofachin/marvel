﻿namespace Marvel.Dto.Checkout
{
    public class ResponseCheckoutDto
    {
        public long CartId { get; set; }

        public bool Open { get; set; }
    }
}
