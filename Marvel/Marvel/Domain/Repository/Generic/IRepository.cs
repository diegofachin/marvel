﻿using Marvel.Domain.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Marvel.Domain.Repository.Generic
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<T> Create(T item);
        Task<T> FindById(long id);
        Task<List<T>> FindAll();
        Task<T> Update(T item);
        Task Delete(long id);
    }
}
