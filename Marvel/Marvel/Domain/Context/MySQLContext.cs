﻿using Marvel.Domain.Entity;
using Microsoft.EntityFrameworkCore;

namespace Marvel.Domain.Context
{
    public class MySQLContext : DbContext
    {
        public MySQLContext()
        {

        }

        public MySQLContext(DbContextOptions<MySQLContext> options) : base(options)
        {

        }

        public DbSet<Cart> Cart { get; set;}
        public DbSet<CartItems> CartItems { get; set; }
    }
}
