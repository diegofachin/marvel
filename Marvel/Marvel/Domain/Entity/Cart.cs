﻿using Marvel.Domain.Base;

namespace Marvel.Domain.Entity
{
    public class Cart : BaseEntity
    {
        public string DiscountCoupon { get; set; }

        public decimal Amount { get; set; } = 0;

        public decimal FinalAmount { get; set; } = 0;

        public bool Open { get; set; }
    }
}
