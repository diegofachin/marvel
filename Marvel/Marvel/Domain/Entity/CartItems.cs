﻿using Marvel.Domain.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Marvel.Domain.Entity
{
    public class CartItems : BaseEntity
    {
        public string Description { get; set; }
        public int CartId { get; set; }
        public int ItemId { get; set; }
        public decimal Price { get; set; }

    }
}
