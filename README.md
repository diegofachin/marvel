Esse é um MVP do projeto Marvel, o mesmo foi estruturado com .Net Core 5, APIs RESTFul e banco de dados Mysql. 
Realiza comunicação com a api externa da Marvel utilizando RestSharp e está comunicando algumas operações básicas com o banco de dados. 
O projeto deverá ser executado diretamente na máquina local, contendo também o banco de dados.

O projeto está organizado em 4 Macro pastas:
- Controllers, onde é encontrado todos os endpoints implementados para a solução.
- Domain, que contem toda a parte relacionada a DB.
- Dto, que contem todos os contratos de request e response dos endpoints
- Services, que são acionados diretamente pelos endpoints do Controller, contém a regra de negócio e definem a ação que ocorrera no DB.

Oque o projeto contempla?
- Comunicação com a API da Marvel que consulta os quadrinhos existentes. 
- Carrinho para adicionar e remover os quadrinhos.
- Checkout para realizar o fechamento dos carrinhos, no momento do fechamento poderá ser informado um cupom de desconto junto ao valor final do carrinho.

Próximos passos para evoluir a solução a partir de definições mais detalhadas sobre a regra de negócio:
- Adicionar o recurso de migrations ao projeto, para que quando surgirem alterações no DB as mesmas ocorram de maneira mais fluída e assim não será mais necessário criar manualmente as entidades e atributos.
- Implementar recurso para listagem de itens de um carrinho.
- Melhorar paginação na listagem de quadrinhos.
- Definir quais filtros serão utilizados na listagem de quadrinhos.
- Colher mais detalhes sobre os quadrinhos raros e seus descontos.
- Adicionar projeto para Testes unitários automatizados (XUNIT ou NUNIT).
- Realizar validações para consistir algumas informações ao executar os endpoints, esse ponto depende de definições na regra de negócio.


==========================================
Script do banco de dados "Marvel"

CREATE TABLE IF NOT EXISTS `cart` (
	`Id` int UNSIGNED AUTO_INCREMENT PRIMARY KEY,,
	`DiscountCoupon` VARCHAR(50) NULL DEFAULT NULL,
	`Amount` DOUBLE NULL DEFAULT NULL,
	`FinalAmount` DOUBLE NULL DEFAULT NULL
	`Open` TINYINT NULL DEFAULT NULL
)
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `cartItemsX` (
	`Id` int UNSIGNED AUTO_INCREMENT PRIMARY KEY,,
	`Description` VARCHAR(200) NULL DEFAULT NULL,	
	`CartId` int NULL DEFAULT NULL,
	`ItemId` int NULL DEFAULT NULL,
	`Price` DOUBLE NULL DEFAULT NULL
)
ENGINE=InnoDB
;
